#include "function.h"

double deg(double x, double e, double a)
{
    int fuc = 1;
    int count = 1;
    double res = 1;
    double variable = x;
    double sus = a;
    do
    {
        res = res + ((sus*variable)/fuc);
        sus = sus*(a-count);
        count++;
        variable = variable*x;
        fuc = fuc*count;
    }while(fabs((sus*variable)/fuc) >= e);
    return res;
}
