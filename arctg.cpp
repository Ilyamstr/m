#include "function.h"

double arctg(double x, double e)
{
    double res = x;
    int i = 3;
    int sign = -1;
    double variable = x*x*x;
    do
    {
        res = res+(sign*(variable/i));
        sign = sign*(-1);
        i = i + 2;
        variable = variable*x*x;

    }while(fabs(variable/i) >= e);
    return res;
}
