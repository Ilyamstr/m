#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "function.h"
#include <QString>
#include <QErrorMessage>
#include <QDoubleValidator>
#include <QRegExp>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    ui->lineEdit_9->setValidator(new QDoubleValidator);
    ui->lineEdit_2->setValidator(new QDoubleValidator);
    ui->lineEdit->setValidator(new QDoubleValidator);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()//eshka
{
    QString res;
    double result;
    QString e = ui->lineEdit_2->text();
    e.replace(QString(","), QString("."));
    double ee = e.toDouble();
    QString x = ui->lineEdit_9->text();
    x.replace(QString(","), QString("."));
    double xx = x.toDouble();
    if (fabs(xx) > 2)
        (new QErrorMessage(this))->showMessage("Error! Slishlom bolshoe x");
    if(ee <= 0)
        (new QErrorMessage(this))->showMessage("Error! Write correct tochnost`");
    else
    {
        result = eshka(xx, ee);
        res.setNum(result);
        ui->lineEdit_3->setText(res);
    }
}

void MainWindow::on_pushButton_6_clicked()
{
    QString res;
    double result;
    QString e = ui->lineEdit_2->text();
    e.replace(QString(","), QString("."));
    double ee = e.toDouble();
    QString x = ui->lineEdit_9->text();
    x.replace(QString(","), QString("."));
    double xx = x.toDouble();
    if (xx < -1 || xx > 1)
        (new QErrorMessage(this))->showMessage("Error! X = [-1; 1]");
    else
    {
        if(ee <= 0)
            (new QErrorMessage(this))->showMessage("Error! Write correct tochnost`");
        else
        {
            result = arctg(xx, ee);
            res.setNum(result);
            ui->lineEdit_8->setText(res);
        }
    }
}

void MainWindow::on_pushButton_3_clicked()
{
    QString res;
    double result;
    QString e = ui->lineEdit_2->text();
    e.replace(QString(","), QString("."));
    double ee = e.toDouble();
    QString x = ui->lineEdit_9->text();
    x.replace(QString(","), QString("."));
    double xx = x.toDouble();

    if(ee <= 0)
        (new QErrorMessage(this))->showMessage("Error! Write correct tochnost`");
    else
    {
        result = cos(xx, ee);
        res.setNum(result);
        ui->lineEdit_5->setText(res);
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    QString res;
    double result;
    QString e = ui->lineEdit_2->text();
    e.replace(QString(","), QString("."));
    double ee = e.toDouble();
    QString x = ui->lineEdit_9->text();
    x.replace(QString(","), QString("."));
    double xx = x.toDouble();

    if(ee <= 0)
        (new QErrorMessage(this))->showMessage("Error! Write correct tochnost`");
    else
    {
        result = sin(xx, ee);
        res.setNum(result);
        ui->lineEdit_4->setText(res);
    }
}

void MainWindow::on_pushButton_4_clicked()
{
    QString res;
    double result;
    QString e = ui->lineEdit_2->text();
    e.replace(QString(","), QString("."));
    double ee = e.toDouble();
    QString x = ui->lineEdit_9->text();
    x.replace(QString(","), QString("."));
    double xx = x.toDouble();
    if (xx <= -1 || xx > 1)
        (new QErrorMessage(this))->showMessage("Error! X = (-1; 1]");
    else
    {
        if(ee <= 0)
            (new QErrorMessage(this))->showMessage("Error! Write correct tochnost`");
        else
            {
            result = ln(xx, ee);
            res.setNum(result);
            ui->lineEdit_6->setText(res);
            }
    }
}

void MainWindow::on_pushButton_5_clicked()
{
    QString res;
    double result;
    QString e = ui->lineEdit_2->text();
    e.replace(QString(","), QString("."));
    double ee = e.toDouble();
    QString x = ui->lineEdit_9->text();
    x.replace(QString(","), QString("."));
    double xx = x.toDouble();
    QString a = ui->lineEdit->text();
    a.replace(QString(","), QString("."));
    double aa = a.toDouble();

    if (xx <= -1 || xx >= 1)
        (new QErrorMessage(this))->showMessage("Error! X = (-1; 1)");
    else
    {
        if(ee <= 0 || aa == NULL)
            (new QErrorMessage(this))->showMessage("Error! Write correct data:\n a = N, e > 0");
        else
        {
            result = deg(xx, ee, aa);
            res.setNum(result);
            ui->lineEdit_7->setText(res);
        }
    }
}
