#include "function.h"

double ln(double x, double e)
{
    double res = x;
    double sign = -1;
    double variable = x*x;
    double num = 2;
    do
    {
        res = res + sign*variable/num;
        variable = variable*x;
        num++;
        sign = sign*(-1);
    }while(fabs(variable/num) >= e);
    return res;
}
