#include "test.h"

void test::test_cos()
{
    double e = 0.1;
    double x = 1;
    QCOMPARE(0.5, cos(x, e));

    double e2 = 0.01;
    double x2 = 0.4;
    QCOMPARE(0.92, cos(x2, e2));
}
