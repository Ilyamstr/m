#include "test.h"

void test::test_arctg()
{
    double e = 1;
    double x = 0.6;
    QCOMPARE(0.528, arctg(x, e));
}
