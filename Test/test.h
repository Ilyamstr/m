#ifndef TEST_H
#define TEST_H

#include <QtCore>
#include <QtTest/QTest>
#include <QtTest>
#include "../function.h"

class test : public QObject
{
    Q_OBJECT

public:
    test();

private slots:
    void test_eshka();
    void test_arctg();
    void test_cos();
    void test_deg();
    void test_ln();
    void test_sin();

};

#endif // TEST_H
