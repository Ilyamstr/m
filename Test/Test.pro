QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_test.cpp \
    test_eshka.cpp \
    mainm.cpp \
    ../arctg.cpp \
    ../cos.cpp \
    ../deg.cpp \
    ../eshka.cpp \
    ../ln.cpp \
    ../sin.cpp \
    test_arctg.cpp \
    test_cos.cpp \
    test_deg.cpp \
    test_ln.cpp \
    test_sin.cpp

HEADERS += \
    test.h \
    function.h
